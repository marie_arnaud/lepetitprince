from Robot.Map import Grid

G1 = Grid(3,3,1,1,1)

G1.print()
G1.right(1)
G1.down(1)
G1.print()

def verif_right():
    G1.print()
    G1.right(1)
    G1.print()

def verif_left():
    G1.print()
    G1.left(1)
    G1.print()
    
def verif_down():
    G1.print()
    G1.down(1)
    G1.print()

def verif_up():
    G1.print()
    G1.up(1)
    G1.print()

#verif_right()
#verif_left()
#verif_down()
#verif_up()


