# LePetitPrince

Le but de ce programme est de permettre le déplacement d'un robot et de le retranscrire dans une grille sous forme de matrice. 
Le robot est capable de se déplacer dans la grille à condition qu'il ne soit pas déjà au bord. 
Plusieurs contraintes ont été rajoutées comme les murs fixes (wall) qui empèchent le robot d'avancer et les murs déplaçables (obstacle) que le robot doit pouvoir pousser en avançant.

Nous avons donc dû gérer plusieurs problèmes: 
    -le fait qu'un mur ne pouvait pas apparaître à l'endroit où un robot a déjà été initialisé
    -le fait qu'un obstacle ne pouvait pas apparaître à l'endroit où un robot ou un mur a déjà été initialisé
    -le fait que deux robots ne pouvaient pas se superposer 
    