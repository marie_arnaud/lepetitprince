from fcntl import DN_DELETE
import numpy as np
import random

# initialisation des paramètres de la classe Grid
class Grid:
    def __init__(self,nb_ligne,nb_colonne,nb_wall,nb_robot,nb_obstacle):
        self.nb_ligne = nb_ligne # nombre de lignes de la grille 
        self.nb_colonne = nb_colonne # nombre de colonnes de la grille 
        self.nb_wall = nb_wall # nombre de murs fixes 
        self.nb_obstacle = nb_obstacle # nombre d'obstacles 
        self.nb_robot = nb_robot # nombre de robots 
        self.grid = np.zeros((nb_ligne,nb_colonne)) # on initialise une grille avec uniquement des zéros
        self.index = [[0]]*nb_robot # on initialise une liste de listes contenant les positions de chaque robot
        for i in range (0,nb_robot): # on initialise la position des robots dans la grilles (première colonne les uns en dessous des autres)
            self.index[i] = [i,0]
            self.grid[i][0] = i+1
        self.wall()
        self.obstacle()

# la fonction obstacle permet de générer des positions aléatoires d'obstacles sur la grille 
# en vérifiant que la position générée n'est pas déjà occupée c'est à dire différente de 0
# si la place est libre alors on pose un obstacle (333)  
    def obstacle(self):
        print("Il y a", self.nb_obstacle, "Obstacle(s)")
        for i in range (0,self.nb_obstacle):
            obstacle_x = 0
            obstacle_y = 0
            while self.grid[obstacle_x][obstacle_y] != 0:
                obstacle_x = random.randint(0,self.nb_ligne-1)
                obstacle_y = random.randint(0,self.nb_colonne-1)
            self.grid[obstacle_x][obstacle_y] = 333

# la fonction wall permet de générer des positions aléatoires de murs sur la grille 
# en vérifiant que la position générée n'est pas déjà occupée c'est à dire différente de 0
# si la place est libre alors on pose un mur (111)
    def wall(self):
        print("Il y a", self.nb_wall, "Wall(s)")
        for i in range (0,self.nb_wall):
            wall_x = 0
            wall_y = 0
            while self.grid[wall_x][wall_y] != 0:
                wall_x = random.randint(0,self.nb_ligne-1)
                wall_y = random.randint(0,self.nb_colonne-1)
            self.grid[wall_x][wall_y] = 111

# la fonction up permet d'effectuer une translation vers le haut du robot
# en s'assurant que le robot n'est pas déjà tout en haut de la grille, qu'il n'y ai pas de mur juste au dessus de lui 
# ou encore que si il y a un obstacle juste au dessus, celui-ci est également translaté vers le haut à condition
# de ne pas être déjà au bord de la grille sinon il ne se passe rien
    def up(self,tag_rb):
        tag_rb = tag_rb-1
        if self.grid[self.index[tag_rb][0]-1][self.index[tag_rb][1]] == 333 and self.grid[self.index[tag_rb][0]-2][self.index[tag_rb][1]] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][0] = self.index[tag_rb][0] - 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            self.grid [self.index[tag_rb][0]-1][self.index[tag_rb][1]] = 333
            return True
        if self.index[tag_rb][0] > 0 and self.grid[self.index[tag_rb][0]-1][self.index[tag_rb][1]] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][0] = self.index[tag_rb][0] - 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            return True
        else:
            return False

# la fonction down permet d'effectuer une translation vers le bas du robot
# en s'assurant que le robot n'est pas déjà tout en bas de la grille, qu'il n'y ai pas de mur juste en dessous de lui 
# ou encore que si il y a un obstacle juste en dessous, celui-ci est également translaté vers le bas à condition
# de ne pas être déjà au bord de la grille sinon il ne se passe rien
    def down(self,tag_rb):
        tag_rb = tag_rb-1
        if self.grid[self.index[tag_rb][0]+1][self.index[tag_rb][1]] == 333 and self.grid[self.index[tag_rb][0]+2][self.index[tag_rb][1]] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][0] = self.index[tag_rb][0] + 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            self.grid [self.index[tag_rb][0]+1][self.index[tag_rb][1]] = 333
            return True
        if self.index[tag_rb][0] < self.nb_ligne and self.grid[self.index[tag_rb][0]+1][self.index[tag_rb][1]] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][0] = self.index[tag_rb][0] + 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            return True
        else:
            return False

# la fonction left permet d'effectuer une translation vers la gauche du robot
# en s'assurant que le robot n'est pas déjà tout à gauche de la grille, qu'il n'y ai pas de mur juste à sa gauche
# ou encore que si il y a un obstacle juste à sa gauche, celui-ci est également translaté vers la gauche à condition
# de ne pas être déjà au bord de la grille sinon il ne se passe rien
    def left(self,tag_rb):
        tag_rb = tag_rb-1
        if self.grid[self.index[tag_rb][0]][self.index[tag_rb][1]-1] == 333 and self.grid[self.index[tag_rb][0]][self.index[tag_rb][1]-2] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][1] = self.index[tag_rb][1] - 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]-1] = 333
            return True
        if self.index[tag_rb][1] > 0 and self.grid[self.index[tag_rb][0]][self.index[tag_rb][1]-1] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][1] = self.index[tag_rb][1] - 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            return True
        else:
            return False

# la fonction right permet d'effectuer une translation vers la droite du robot
# en s'assurant que le robot n'est pas déjà tout à droite de la grille, qu'il n'y ai pas de mur juste à sa droite
# ou encore que si il y a un obstacle juste à sa droite, celui-ci est également translaté vers la droite à condition
# de ne pas être déjà au bord de la grille sinon il ne se passe rien
    def right(self,tag_rb):
        tag_rb = tag_rb-1
        if self.grid[self.index[tag_rb][0]][self.index[tag_rb][1]+1] == 333 and self.grid[self.index[tag_rb][0]][self.index[tag_rb][1]+2] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][1] = self.index[tag_rb][1] + 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]+1] = 333
        if self.index[tag_rb][1] < self.nb_colonne and self.grid[self.index[tag_rb][0]][self.index[tag_rb][1]+1] == 0:
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = 0
            self.index[tag_rb][1] = self.index[tag_rb][1] + 1
            self.grid [self.index[tag_rb][0]][self.index[tag_rb][1]] = tag_rb+1
            return True
        else:
            return False

# la fonction print permet d'afficher la grille
    def print(self):
        for i in range (0,self.nb_ligne):
            print("| ", self.grid[i], " |")
        print("----------------") 
    